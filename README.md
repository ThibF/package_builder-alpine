# Alpine package builder
Rather then having to copy each file onto the target manually, instead it is
also possible to use an package manager. This repository contains a few scripts
to generate a docker container to build Alpine packages (apk).

This repository generates a container to be used from a CI system.

Note, that the purpose of this repository is NOT to create upstream Alpine
packages nor to compile packages using the Alpine packaging build system.

## Using this repository
This repository is not intended to be used directly. Instead, the docker image
create from this registry should be used instead. Alternatively this repository
could be forked, and used to push images to a different registry, or to build
local docker images.

### Releases and tagging
To create a release, an annotated tag is to be used. A tag shall have the form
of 'vX.Y[.Z][-rcN]'.
* X required major version of the release, can be any integer value, including a year
* Y required minor version of the release, can be any integer value, including month
* Z optional hotfix version of the release only used in release branches
* N optional release candidate of the release, can be any integer value

To create a release branch, tag the master branch with the major and minor
components only.
So for example *v0.10*. The CI will then create the needed branches and tags
on a successful build.

It is strongly preferred to use the local ***git_tag_release.sh*** script, as it
not only wraps around the regular git tag command, but also updates the
*CHANGELOG.md* file.

An important note about merging, the CI will do updates and merges across all
branches based from the stable/vX.Y/releases branch. As such, merges into the
release branches should be avoided, as conflicts cannot be resolved by the CI.

To avoid race conditions, ensure that not two tags are pushed before other
tasks have finished in the CI. This mostly is a problem when many concurrent
tasks are enabled and thus run in parallel on the CI.

The recommended strategy is thus to fork from master by creating the branch tag,
and fixes that need to be applied to other branches, for example a fix already
on master, is to be cherry picked. Fixes from a release branch back to master
should follow the normal branch -> merge (into master) principle.

#### Release candidates
The release branch will also be taged with an release candidate status.
For the previous example of release tag *v0.10* a tag *v0.10.0-rc1* will be
created.

#### Final release
If after a certain amount of time, the release candidate is deemed ready for
release, it shall be tagged with its release tag. For example *v0.10.0* to mark
*v0.10.0-rc1* as to be released. In other words, the -rc tag is to be removed.

#### Hotfixes
Within the main release branch (stable/v0.10/library) hotfix patches can be
applied.
If a release candidate is desired for a certain number of hotfixes, tag the
release branch as such, for example *v0.10.0-rc1*. If there has been a hotfix
release already, then the numbering should take this into account. For example
*v0.10.1-rc0* and so if another release candidate is to follow the tag would be
*v0.10.1-rc1*

 To create a new release with these hotfixes, a new tag is to be created
in the form of *v0.10.1* to release *v0.10.1-rc1*.

#### Annotated tags
Annotated tags are required, as all scripts will ignore light tags. The tag
subject will be used as the release name, and the tag body will be used as
release notes. As such it is strongly recommended to use messaged tags.

#### Automated changelogs
The *CHANGELOG.md* file is to be updated automatically using the
`git_tag_release.sh` script, optionally after creating the desired tag.

The `git_tag_release.sh` script will take the tag and its message, generate
the relevant *CHANGELOG.md* file if needed and commit it and rewrite the tag to
point to the new commit. All arguments passed are those of git tag. If no
arguments are passed, the current HEAD will be evaluated if it points to a tag
and if not, fail.

Note, that only full tags are considered in the CHANGELOG, not any '-rc' tag.

#### Hooks
As hosted environment do not allow installing pre-receive hooks, we can only
ensure proper tags are pushed. A proper tag always points to an commit updating
the 'CHANGELOG.md* file. Included in this repository is a git *pre-push* hook
that checks if the supplied tag follows this rule. Everybody that has the
ability and permission to create any release tag (see above) will
need to either do this completely manually (not recommended) or use the
preferred method by installing the supplied *pre-push* hook from the '.githooks'
directory by either manually copying the file to '.git/hooks' or tell git to use
the .githooks directory instead allowing for version managed and updated hooks
(strongly recommended).
```console
git config --local core.hooksPath .githooks
```

## Scripts
The repository contains two scripts,  to create an alpine package and validate
the build environment. While not recommended, if a native build is preferred,
the 'buildenv_check.sh' scrip can be used to validate if a package can be built.

Otherwise, the main script, 'package_builder-alpine.sh' should be run and used
in the generated docker container.

### Prerequisites
This repository only requires docker and a few standard shell tools. The build
script will run a few self-tests before attempting to build anything.

### Usage
To create a local docker container for use, calling the 'docker_build.sh' script
is enough. Tagging and pushing to a registry manually is not done by the script
as this is normally handled by the CI. For now the CI will only push docker
images to the registry if the branch is prefixed with the 'stable/' tag.

## Docker image
The created image can be used to use the Alpine build tools. So for example to
call *newapkbuild* only prefixing it with
`docker run --rm -it <container_name> newapkbuild` is needed. Note however that
as is normal with docker, files inside the container remain inside. This can be
solved by using bind-mounts or using docker to copy the files in/out. See the
docker manual for more information.

# Key management
The 'packager_builder.sh' script will try to use the keypair found in
*${HOME}/.abuild* by copying the public key into /etc/apk/keys. If this is not
intended or desired, ensure that keys are properly installed and this step will
be skipped.
