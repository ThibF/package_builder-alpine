# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

# For Alpine, latest is actually the latest stable
# hadolint ignore=DL3007
FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# We want the latest stable version from the repo
# hadolint ignore=DL3018
RUN \
    apk add --no-cache \
        alpine-sdk \
    && \
    rm -rf "/var/cache/apk/"* && \
    adduser root abuild && \
    adduser -D -G abuild buildbot && \
    chgrp abuild "/etc/apk/keys/" && \
    chmod g+w "/etc/apk/keys"

COPY "./bin/package_builder-alpine.sh" "/usr/local/bin/package_builder-alpine.sh"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/docker-entrypoint.sh" "/init"

ENTRYPOINT [ "/init" ]
