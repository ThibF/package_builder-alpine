#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

WORKDIR="${WORKDIR:-/workdir}"
REQUIRED_COMMANDS="
    [
    basename
    command
    docker
    echo
    eval
    exit
    getopts
    hostname
    id
    printf
    pwd
    readlink
    test
"


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

usage()
{
    echo "Uage: ${0} [OPTIONS]"
    echo "A wrapper script to instantiate the package builder."
    echo "    -e  Environment variable to pass (Ex. VAR=value)"
    echo "    -h  Print usage"
    echo "    -i  Supply a custom registry image [CI_REGISTRY_IMAGE]"
    echo "    -p  Packing build path [PACKAGER_PATH]"
    echo "    -r  Supply a comma separated list of repositories to use (prefix with comma to append existing)"
    echo "    -v  Mount volumes to pass (Ex. /tmp)"
    echo
    echo "All options can also be passed in environment variables (listed between [brackets])."
}

init()
{
    src_file="$(readlink -f "${0}")"
    src_dir="${src_file%%${src_file##*/}}"

    if ! docker pull "${CI_REGISTRY_IMAGE:=unknown}" 2> "/dev/null"; then
        e_warn "Unable to pull docker image '${CI_REGISTRY_IMAGE}', building locally instead."
        # shellcheck disable=SC2021  # Busybox tr is non-posix without classes
        CI_REGISTRY_IMAGE="$(basename "${src_dir}" | tr '[A-Z]' '[a-z]'):latest"
        if ! docker build --pull -t "${CI_REGISTRY_IMAGE}" "${src_dir}"; then
            e_warn "Failed to build local container, attempting existing container."
        fi
    fi

    if ! docker inspect --type image "${CI_REGISTRY_IMAGE}" 1> "/dev/null"; then
        e_err "Container '${CI_REGISTRY_IMAGE}' not found, cannot continue."
        exit 1
    fi

    opt_args="${OPT_ENV:-} -v '${PACKAGER_PATH:-$(pwd)}:${WORKDIR}' ${OPT_VOLUME:-}"

    for _repository in $(echo "${repositories:-}" | tr -s ' ,\f\n\t\v' ' '); do
        if [ -z "${_repository}" ]; then
            continue
        fi

        if [ "${_repository%~*}" != "${_repository}" ]; then
            eval _repository="${_repository}"
        fi

        if [ -d "${_repository}" ]; then
            _repository="$(readlink -f "${_repository}")"
            opt_args="${opt_args:-} -v '${_repository}:${_repository}'"
        fi

        if [ -z "${_repositories:-}" ]; then
            _repositories="${_repository}"
        else
            _repositories="${_repositories},${_repository}"
        fi
    done
    repositories="${_repositories:-}"
}

cleanup()
{
    if [ -d "output/" ]; then
        run_script chown "$(id -u):$(id -g)" -R "output/"
    fi

    trap EXIT
}

run_script()
{
    eval docker run \
                --rm \
                -h "$(hostname)" \
                -i \
                -t \
                -w "${WORKDIR}" \
                "${opt_args:-}" \
                "${CI_REGISTRY_IMAGE}" \
                "${@}"
}

check_requirements()
{
    for _cmd in ${REQUIRED_COMMANDS}; do
        if ! _test_result="$(command -V "${_cmd}")"; then
            _test_result_fail="${_test_result_fail:-}${_test_result}\n"
        else
            _test_result_pass="${_test_result_pass:-}${_test_result}\n"
        fi
    done

    if [ -n "${_test_result_fail:-}" ]; then
        e_err "Self-test failed, missing dependencies."
        echo "======================================="
        echo "Passed tests:"
        # As the results contain \n, we expect these to be interpreted.
        # shellcheck disable=SC2059
        printf "${_test_result_pass:-none\n}"
        echo "---------------------------------------"
        echo "Failed tests:"
        # shellcheck disable=SC2059
        printf "${_test_result_fail:-none\n}"
        echo "======================================="
        exit 1
    fi
}

main()
{
    while getopts ":e:hi:p:r:v:" options; do
        case "${options}" in
        e)
            OPT_ENV="${OPT_ENV:-} -e '${OPTARG}'"
            ;;
        h)
            usage
            exit 0
            ;;
        i)
            CI_REGISTRY_IMAGE="${OPTARG}"
            ;;
        p)
            PACKAGER_PATH="${OPTARG}"
            ;;
        r)
            repositories="${OPTARG}"
            if [ "${repositories#,}" != "${repositories}" ]; then
                append_repositories="true"
            fi
            ;;
        v)
            OPT_VOLUME="${OPT_VOLUME:-} -v '${OPTARG}:${OPTARG}'"
            ;;
        :)
            e_err "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            e_err "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"
    trap cleanup EXIT

    check_requirements
    init
    run_script "${@}" "${repositories:+-r ${append_repositories:+,}${repositories}}"
    cleanup
}

main "${@}"

exit 0
