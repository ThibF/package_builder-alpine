#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Alpine Linux package creator helper script."
    echo "    -h  Print usage"
    echo "    -r  Supply a comma separated list of repositories to use (prefix with comma to append existing) [REPOSITORIES]"
    echo
    echo "All options can also be passed in environment variables (listed between [brackets])."
}

prepare()
{
    if [ -n "${REPOSITORIES:-}" ]; then
        if [ "${REPOSITORIES#,}" == "${REPOSITORIES}" ]; then
            echo "# List of repositories manually given at build time" > "/etc/apk/repositories"
        fi
        echo "${REPOSITORIES:-}" | tr -s ' ,\f\n\t\v' '\n'  >> "/etc/apk/repositories"
    fi

    if git rev-parse --show-cdup 2> "/dev/null"; then
        _untracked_files="$(git ls-files --others)"
        if ! git diff-index --name-only --quiet HEAD -- || [ -n "${_untracked_files:-}" ]; then
            dirty="yes"
        fi

        if ! _pkgver="$(git describe --abbrev=0 2> "/dev/null")"; then
            _pkgver="v0.0.1"
        fi

        if [ "${_pkgver##*-rc}" != "${_pkgver}" ]; then
            _pkgrc="${_pkgver##*-rc}"
        fi

        _pkgver="${_pkgver%%-rc*}${dirty:+d}"
        _pkgver="${_pkgver##*/}${_pkgrc:+_rc${_pkgrc}}"

        _patches="$(git rev-list "$(git describe --abbrev=0)"..HEAD --count 2> "/dev/null")"
        if [ "${_patches}" -gt 0 ]; then
            _pkgver="${_pkgver}_p${_patches}"
        fi
    fi

    if [ -f "APKBUILD.in" ]; then
        sed 's|@PKGVER@|'"${_pkgver:-v0.0.0}"'|g' "APKBUILD.in" > "APKBUILD"
    fi

    if [ ! -f "APKBUILD" ]; then
        e_err "No 'APKBUILD' file found."
        exit 1
    fi

    for _key in "${HOME}/.abuild/"*".rsa.pub"; do
        if [ ! -f "${_key}" ]; then
            abuild-keygen -a -n
        fi
        break
    done

    for _key in "${HOME}/.abuild/"*".rsa.pub"; do
        if [ ! -f "/etc/apk/keys/$(basename "${_key}")" ] && \
           ! cp "${_key}" "/etc/apk/keys/"; then
                e_err "Unable to find '${_key}' in /etc/apk/keys."
                e_err "Please copy the needed public key into /etc/apk/keys/"
                e_err "when not using the recommended docker environment."
                exit 1
        fi
    done
}

build_package()
{
    abuild -F checksum
    apk update
    nice -n 19 abuild -F -r
}

verify_package()
{
    echo "Verifying generated packages"
    find "${HOME}/packages/" -type f -iname "*.apk" -exec apk verify "{}" \;
}

main()
{
    while getopts ":hr:" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        r)
            REPOSITORIES="${OPTARG}"
            ;;
        :)
            e_err "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            e_err "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    prepare
    build_package
    verify_package

    if [ -d "output/" ]; then
        rm -rf "output/"
    fi
    cp -a "${HOME}/packages/" "output/"
}

main "${@}"

exit 0
